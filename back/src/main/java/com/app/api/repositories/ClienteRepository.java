package com.app.api.repositories;

import com.app.api.domains.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    /**
     * readOnly não necessita ser envolvida com uma transação com o DB
     * faz ficar mais rápida e diminui o tempo de locking
     * */
    @Transactional
    Cliente findByEmail(String email);
}
