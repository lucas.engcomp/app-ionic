package com.app.api.services;

import com.app.api.domains.Pedido;
import com.app.api.repositories.PedidoRepository;
import com.app.api.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository repository;

    public Pedido find(Integer id) {
        Optional<Pedido> obj = repository.findById(id);

        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto Não encontrado. Id: " + id + ", do tipo: " + Pedido.class.getName()));
    }

}
