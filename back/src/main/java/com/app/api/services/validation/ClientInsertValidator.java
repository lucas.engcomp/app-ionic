package com.app.api.services.validation;

import com.app.api.domains.Cliente;
import com.app.api.domains.enums.TipoCliente;
import com.app.api.dto.ClienteNewDTO;
import com.app.api.repositories.ClienteRepository;
import com.app.api.resources.exception.FieldMessage;
import com.app.api.services.validation.utils.BR;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;


public class ClientInsertValidator implements ConstraintValidator<ClientInsert, ClienteNewDTO> {

    @Autowired
    private ClienteRepository repository;

    @Override
    public void initialize(ClientInsert ann) {
    }

    /**
     * Description: Método da interface ConstraintValidator que verifica se o tipo que vem como argumento: (ClienteNewDTO) é válido(true) ou não(false)
     */
    @Override
    public boolean isValid(ClienteNewDTO objDto, ConstraintValidatorContext context) {

        List<FieldMessage> list = new ArrayList<>();

        if (objDto.getTipo().equals(TipoCliente.PESSOAFISICA.getCod()) && !BR.isValidCPF(objDto.getCpfOuCnpj())) {
            list.add(new FieldMessage("cpfOuCnpj", "O CPF é inválido ou não existe"));
        }

        if (objDto.getTipo().equals(TipoCliente.PESSOAJURIDICA.getCod()) && !BR.isValidCNPJ(objDto.getCpfOuCnpj())) {
            list.add(new FieldMessage("cpfOuCnpj", "O CNPJ é inválido ou não existe"));
        }

        Cliente aux = repository.findByEmail(objDto.getEmail());
        if (aux != null) {
        list.add(new FieldMessage("email", "Esse email já está sendo usado!"));
        }

        for (FieldMessage e : list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }

        return list.isEmpty();
    }
}